# mqtt_message_toDB



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ramsenorham/mqtt_message_todb.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

Um das Java-Projekt mit der angegebenen pom.xml-Datei in der Apache NetBeans IDE zu öffnen, können Sie die folgenden Schritte befolgen:
1.	Öffnen Sie die NetBeans IDE: Starten Sie die Apache NetBeans IDE auf Ihrem Computer.
2.	Projekt öffnen: Wählen Sie im Menü "File" (Datei) die Option "Open Project..." (Projekt öffnen...).
3.	Navigieren Sie zum Projektverzeichnis: Suchen Sie das Verzeichnis, in dem Sie das Git-Projekt geclont haben, und wählen Sie es aus.
4.	Wählen Sie die pom.xml-Datei aus: Innerhalb des Projektverzeichnisses sollten Sie die pom.xml-Datei finden. Wählen Sie diese Datei aus und klicken Sie auf "Open" (Öffnen).
5.	Projekt wird geladen: NetBeans wird das Projekt laden und die Abhängigkeiten herunterladen.
6.	Projektstruktur anzeigen: Nachdem das Projekt geladen wurde, sollte die Projektstruktur in der NetBeans IDE sichtbar sein. Sie können die verschiedenen Dateien und Ordner im Projektfenster sehen.
7.	Falls erforderlich, Hauptklasse festlegen: In der pom.xml-Datei ist bereits die exec.mainClass angegeben. Stellen Sie sicher, dass die Klasse mqtt_message.Mqtt_message vorhanden ist. NetBeans sollte dies automatisch erkennen, wenn die Struktur des Projekts den Standards entspricht.
8.	Build und Run: Sie können das Projekt jetzt bauen und ausführen. Klicken Sie dazu auf das grüne "Play"-Symbol oder verwenden Sie die Optionen im Menü, um das Projekt zu bauen und auszuführen.

NetBeans sollte automatisch Maven verwenden, um die Abhängigkeiten herunterzuladen und das Projekt zu konfigurieren. Stellen Sie sicher, dass Sie eine Internetverbindung haben, damit Maven die benötigten Bibliotheken herunterladen kann.

## Test and Deploy

### Debian 10 auf virtuelle Maschine VirtualBox installieren:

Um Debian 10 in einer VirtualBox-Umgebung zu installieren, können Sie die folgenden Schritte befolgen:
Schritte zur Installation von Debian 10 in VirtualBox:

#### 1. VirtualBox herunterladen und installieren:
• 1. Download: Gehen Sie zur VirtualBox-Website und laden Sie die neueste Version von VirtualBox für Ihr Betriebssystem herunter.

• 2. Installation: Führen Sie den Installationsassistenten aus und befolgen Sie die Anweisungen, um VirtualBox auf Ihrem Computer zu installieren.

#### 2. Debian 10 ISO-Datei herunterladen:
• 1. Download: Besuchen Sie die Debian-Website und laden Sie die ISO-Datei für Debian 10 (Buster) herunter. Wählen Sie die passende Architektur für Ihre VirtualBox-Installation aus (z.B., 64-bit).

#### 3. Virtual Machine erstellen:
• 1. Starten Sie VirtualBox: Starten Sie VirtualBox und klicken Sie auf "New" (Neu), um eine neue virtuelle Maschine zu erstellen.

• 2. Geben Sie einen Namen ein: Geben Sie einen Namen für Ihre virtuelle Maschine ein (z.B., "Debian 10").

• 3. Wählen Sie den Typ und die Version: Wählen Sie als Typ "Linux" und als Version "Debian (64-bit)" aus.

• 4. RAM zuweisen: Weisen Sie der virtuellen Maschine genügend RAM zu. Debian 10 funktioniert gut mit mindestens 2 GB RAM.

• 5. Erstellen Sie eine virtuelle Festplatte: Wählen Sie die Option "Create a virtual hard disk now" (Jetzt eine virtuelle Festplatte erstellen) und klicken Sie auf "Create" (Erstellen).

• 6. Wählen Sie den Festplattentyp: Lassen Sie die Standardoption "VDI (VirtualBox Disk Image)" ausgewählt und klicken Sie auf "Next" (Weiter).

• 7. Wählen Sie die Speicherart: Wählen Sie "Dynamically allocated" (Dynamisch allokiert) aus und klicken Sie auf "Next" (Weiter).

• 8. Größe der virtuellen Festplatte festlegen: Legen Sie die Größe der virtuellen Festplatte fest. Mindestens 25 GB sollten für Debian 10 ausreichen.

• 9. ISO-Datei hinzufügen: Klicken Sie auf "Settings" (Einstellungen) und dann auf "System". Wählen Sie im Abschnitt "Motherboard" die Option "Enable EFI (special OSes only)" aus. Klicken Sie dann auf "Storage", wählen Sie den leeren Controller unter "Controller: IDE" aus, und klicken Sie auf das Diskettensymbol neben "Optical Drive". Wählen Sie die Debian 10 ISO-Datei aus.

• 10. Starten Sie die virtuelle Maschine:	Klicken Sie auf "Start" (Starten), um die virtuelle Maschine zu starten.

#### 4. Debian 10 installieren:
• 1. Wählen Sie die Installationsmethode: Wählen Sie die Installationsmethode aus (z.B., "Install").

• 2. Folgen Sie den Installationsanweisungen: Befolgen Sie die Anweisungen des Debian-Installationsprogramms. Dies umfasst das Festlegen von Sprache, Ort, Tastaturbelegung, Benutzername und Passwort.

• 3. Partitionierung: Wählen Sie eine Partitionierungsmethode, die Ihren Anforderungen entspricht. Die einfachste Option ist normalerweise "Guided - use entire disk".

• 4. Grub installieren: Wenn Sie dazu aufgefordert werden, installieren Sie den Grub-Bootloader auf die Festplatte.

• 5. Fertigstellung der Installation: Nach Abschluss der Installation starten Sie die virtuelle Maschine neu.

### PostgreSQL auf Debian 10 installieren:

1.	Aktualisieren Sie Ihr System:
```bash
sudo apt update 
sudo apt upgrade
```
2.	Installieren Sie PostgreSQL:
```bash
sudo apt install postgresql postgresql-contrib
```
3.	Starten Sie den PostgreSQL-Dienst:
```bash
sudo service postgresql start
```
4.	Überprüfen Sie den Status (optional):
```bash
sudo service postgresql status
```
#### Remote-Verbindungen für PostgreSQL konfigurieren:
1.	Öffnen Sie die PostgreSQL-Konfigurationsdatei:
```bash
sudo nano /etc/postgresql/{version}/main/postgresql.conf
```
Ersetzen Sie {version} durch die PostgreSQL-Version, die Sie installiert haben (z.B., 11).
2.	Ändern Sie die listen_addresses: Suchen Sie die Zeile mit listen_addresses und stellen Sie sicher, dass sie wie folgt aussieht:
```conf
listen_addresses = '*'
```
Dies ermöglicht Verbindungen von allen IP-Adressen.
3.	Speichern Sie die Datei und schließen Sie sie: Drücken Sie Ctrl + X, dann Y, und anschließend Enter.
4.	Konfigurieren Sie das Hinzufügen von Host-basierten Authentifizierungsmethoden:
```bash
sudo nano /etc/postgresql/{version}/main/pg_hba.conf
```
Fügen Sie eine Zeile hinzu, um Remote-Verbindungen zu erlauben. Zum Beispiel:
```conf
host all all 0.0.0.0/0 md5
```
Diese Zeile erlaubt Verbindungen aus allen IP-Adressen (0.0.0.0/0). Passen Sie dies nach Bedarf an, um die Sicherheit zu verbessern.
5.	Speichern Sie die Datei und schließen Sie sie: Drücken Sie Ctrl + X, dann Y, und anschließend Enter.
6.	Starten Sie PostgreSQL neu, um die Änderungen zu übernehmen:
```bash
sudo service postgresql restart
```
#### PostgreSQL-Benutzer und Datenbank erstellen:
1.	Verbinden Sie sich mit PostgreSQL:
```bash
sudo -u postgres psql
```
2.	Erstellen Sie einen Benutzer (ersetzen Sie youruser und yourpassword):
```sql
CREATE USER youruser WITH PASSWORD 'yourpassword';
```
3.	Erstellen Sie eine Datenbank (ersetzen Sie yourdatabase):
```sql
CREATE DATABASE yourdatabase;
```
4.	Weisen Sie dem Benutzer Berechtigungen für die Datenbank zu:
```sql
GRANT ALL PRIVILEGES ON DATABASE yourdatabase TO youruser;
``` 
5.	Verlassen Sie die PostgreSQL-Sitzung:
```sql
\q
```
#### Firewall konfigurieren (falls erforderlich):
1.	Wenn Sie eine Firewall verwenden (z.B., UFW), öffnen Sie den PostgreSQL-Port (standardmäßig 5432):
```bash
sudo ufw allow 5432
```
2.	Aktivieren Sie die Firewall-Regel:
```bash
sudo ufw enable
```
#### phpPgAdmin auf Windows 11-System:

1. PostgreSQL auf Debian 10 konfigurieren: Stellen Sie sicher, dass PostgreSQL auf der Debian 10 VM ordnungsgemäß konfiguriert ist, wie in den vorherigen Anweisungen beschrieben.
2. PostgreSQL für Remote-Zugriff konfigurieren: Folgen Sie den Schritten zur Konfiguration von Remote-Verbindungen für PostgreSQL auf der Debian 10 VM, wie zuvor erläutert.
3. Firewall-Einstellungen überprüfen: Vergewissern Sie sich, dass die Firewall auf der Debian 10 VM den PostgreSQL-Port (standardmäßig 5432) zulässt.
```bash
sudo ufw allow 5432 
sudo ufw enable
```
4. phpPgAdmin auf Windows 11 installieren:

• Laden Sie die neueste Version von phpPgAdmin herunter.

• Entpacken Sie das Archiv an einem geeigneten Ort auf Ihrem Windows-System.

5. phpPgAdmin konfigurieren:

• Navigieren Sie zum Verzeichnis, in dem Sie phpPgAdmin entpackt haben.

• Kopieren Sie die Datei config.inc.php-dist und benennen Sie sie in config.inc.php um.

• Öffnen Sie config.inc.php in einem Texteditor und konfigurieren Sie die Verbindung zu Ihrer PostgreSQL-Datenbank:
```php
// Set the default server (0 = no default)
$conf['servers'][0]['host'] = 'IP_DEINER_DEBIAN_VM';
$conf['servers'][0]['port'] = 5432;
$conf['servers'][0]['sslmode'] = 'allow';
$conf['servers'][0]['pg_dump_path'] = '/usr/bin/pg_dump';
$conf['servers'][0]['pg_dumpall_path'] = '/usr/bin/pg_dumpall';
``` 
Ersetzen Sie 'IP_DEINER_DEBIAN_VM' durch die tatsächliche IP-Adresse oder den Hostnamen Ihrer Debian 10 VM.
6. Webserver konfigurieren: Da phpPgAdmin über einen Webbrowser aufgerufen wird, benötigen Sie einen Webserver. Hier können Sie entweder Apache, Nginx oder einen anderen Webserver Ihrer Wahl verwenden.

• Installieren Sie einen Webserver Ihrer Wahl: Apache
``` bash
sudo apt install apache2
```  
• Kopieren Sie das phpPgAdmin-Verzeichnis in das Webserver-Wurzelverzeichnis:
``` bash
sudo cp -r /pfad/zu/phppgadmin /var/www/html
```  
7. phpPgAdmin aufrufen:
Öffnen Sie einen Webbrowser auf Ihrem Windows 11-System und navigieren Sie zur Adresse http://IP_DEINER_DEBIAN_VM/phppgadmin. Sie sollten die phpPgAdmin-Benutzeroberfläche sehen und sich mit den PostgreSQL-Anmeldeinformationen anmelden können.
Stellen Sie sicher, dass die Firewall auf der Debian 10 VM den Verkehr zum Webserver-Port zulässt (standardmäßig 80 für HTTP).

#### Backup mit pgAdmin:

1.	Navigieren Sie zu 'Databases': Erweitern Sie im Server-Objektbaum den Ordner "Databases".
2.	Wählen Sie die 'mqtt_db' Datenbank aus: Suchen Sie nach der 'mqtt_db'-Datenbank in der Liste und klicken Sie darauf, um sie auszuwählen.
3.	Öffnen Sie das Kontextmenü und wählen Sie "Backup": Klicken Sie mit der rechten Maustaste auf die ausgewählte Datenbank ('mqtt_db') und wählen Sie "Backup..." aus dem Kontextmenü.
4.	Konfigurieren Sie die Backup-Optionen:

•	Im Backup-Dialog können Sie verschiedene Optionen konfigurieren, wie z.B. den Dateipfad für das Backup und die Art des Backups (Plain oder Custom).

•	Wählen Sie einen geeigneten Dateipfad und geben Sie einen Dateinamen für das Backup an.

5.	Wählen Sie die Art des Backups:

•	Wenn Sie ein einfaches SQL-Backup möchten, wählen Sie "Plain" aus.

•	Wenn Sie benutzerdefinierte Optionen wie Kompression oder Verschlüsselung verwenden möchten, wählen Sie "Custom" aus und konfigurieren Sie die gewünschten Optionen.

6.	Starten Sie das Backup: Klicken Sie auf "Backup" oder "OK", um das Backup zu starten.
7.	Überprüfen Sie den Erfolg des Backups: Nach Abschluss des Backups sehen Sie eine Erfolgsmeldung. Sie können auch den Dateipfad überprüfen, um sicherzustellen, dass die Backup-Datei erstellt wurde.

Alternative: Verwenden Sie pg_dump auf der Kommandozeile:

Wenn Sie die Backup-Operationen lieber auf der Kommandozeile durchführen möchten, können Sie das pg_dump-Dienstprogramm verwenden.
```bash
pg_dump --host "192.168.178.44" --port "5432" --username "postgres" --no-password --verbose --format=c --blobs --create --clean --inserts --column-inserts --dbname "mqtt_db" --file "/path/to/backup_file.backup" 
```
Hier eine kurze Erklärung der Argumente:

•	--host "192.168.178.44": Die IP-Adresse oder der Hostname des PostgreSQL-Servers.

•	--port "5432": Der Port, auf dem der PostgreSQL-Server läuft (standardmäßig 5432).

•	--username "postgres": Der Benutzername, mit dem Sie sich am PostgreSQL-Server anmelden möchten.

•	--no-password: Die Option, die besagt, dass keine Passwortabfrage erfolgen soll. Beachten Sie, dass dies aus Sicherheitsgründen vermieden werden sollte und besser mit Passwort gearbeitet werden sollte.

•	--verbose: Zeigt detaillierte Informationen über den Fortschritt des Dump-Prozesses an.

•	--format=c: Gibt an, dass das benutzerdefinierte Format für das Backup verwendet werden soll.

•	--blobs: Inkludiert BLOBs (Binary Large Objects) im Backup.

•	--create: Enthält CREATE TABLE-Anweisungen im Backup.

•	--clean: Fügt DROP TABLE-Anweisungen vor den CREATE TABLE-Anweisungen im Backup ein.

•	--inserts: Verwendet INSERT-Anweisungen für Daten.

•	--column-inserts: Verwendet separate INSERT-Anweisungen für jede Spalte.

Beachten Sie, dass Sie den Pfad zur Backup-Datei (--file "/path/to/backup_file.backup") an Ihre spezifischen Anforderungen anpassen sollten. Stellen Sie sicher, dass der Benutzer, der das pg_dump-Kommando ausführt, über die erforderlichen Berechtigungen verfügt, um auf das Zielverzeichnis schreiben zu können.

### Broker installieren:

MQTT-Broker auf Debian 10 installieren:
in Virtual Machine (VM) können Sie den Mosquitto-Broker verwenden. Hier sind die Schritte, um Mosquitto auf Debian 10 zu installieren und zu konfigurieren:

#### 1. Mosquitto-Broker installieren:
1.	Aktualisieren Sie das System:
```bash
sudo apt update sudo apt upgrade
```
2.	Installieren Sie den Mosquitto-Broker und -Client:
```bash
sudo apt install mosquitto mosquitto-clients 
```
#### 2. Mosquitto-Broker konfigurieren:
1.	Starten Sie den Mosquitto-Dienst:
```bash
sudo systemctl start mosquitto
```
2.	Aktivieren Sie den Mosquitto-Dienst, um sicherzustellen, dass er beim Systemstart gestartet wird:
```bash
sudo systemctl enable mosquitto 
```
#### 3. Optional: Firewall-Konfiguration:
Wenn Sie eine Firewall auf Ihrer Debian 10 VM verwenden, öffnen Sie den Mosquitto-Port (standardmäßig 1883):
```bash
sudo ufw allow 1883 
sudo ufw enable 
```
#### 4. Testen Sie den Mosquitto-Broker:
• 1. Verbinden Sie sich mit dem Broker: Öffnen Sie zwei Terminalfenster und verwenden Sie eines davon, um sich mit dem Broker zu verbinden:
```bash
mosquitto_sub -h localhost -t test
```
• 2. Senden Sie eine Testnachricht: Verwenden Sie das andere Terminalfenster, um eine Testnachricht zu senden:
```bash
mosquitto_pub -h localhost -t test -m "Hello, MQTT!"
```
Sie sollten die Nachricht im ersten Terminalfenster sehen.

#### 5. Konfiguration für Remote-Zugriff (optional):
Wenn Sie den MQTT-Broker für Remote-Zugriff konfigurieren möchten, müssen Sie die mosquitto.conf-Datei anpassen. Beachten Sie dabei Sicherheitsaspekte, insbesondere wenn Ihr Broker öffentlich erreichbar ist.
1.	Öffnen Sie die Konfigurationsdatei in einem Texteditor:
```bash
sudo nano /etc/mosquitto/mosquitto.conf
```
2.	Fügen Sie die folgenden Zeilen hinzu, um Remote-Zugriff zu ermöglichen:
```conf
listener 1883
protocol mqtt # Erlauben Sie anonymen Zugriff (passen Sie nach Bedarf an): 
allow_anonymous true
```
3.	Speichern Sie die Datei und starten Sie den Mosquitto-Dienst neu:
```bash
sudo systemctl restart mosquitto
```

### MQTT.fx Installieren:
1.	Besuchen Sie die MQTT.fx-Website: Gehen Sie zur offiziellen Website von MQTT.fx und laden Sie die neueste Version des Tools herunter.
2.	Installationsprozess:

•	Führen Sie das Installationsprogramm aus.

•	Wählen Sie den Installationspfad und bestätigen Sie die Installation.
#### MQTT.fx Konfigurieren:
1.	Starten Sie MQTT.fx: Nach der Installation starten Sie MQTT.fx.
2.	Broker hinzufügen:

•	Klicken Sie auf "Connection" (Verbindung) oder ein ähnlich benanntes Menü.

•	Wählen Sie "Add Connection" (Verbindung hinzufügen).

3.	Broker-Konfiguration:

•	Geben Sie die erforderlichen Informationen ein, wie Host, Port und ggf. Anmeldeinformationen.

•	Stellen Sie sicher, dass Sie die richtigen Einstellungen für Ihren MQTT-Broker verwenden.

4.	Verbindung herstellen: Klicken Sie auf "Connect" (Verbinden), um eine Verbindung zum Broker herzustellen.
5.	Topic abonnieren oder veröffentlichen: Klicken Sie auf "Subscribe" (Abonnieren), um ein Thema zu abonnieren, oder auf "Publish" (Veröffentlichen), um eine Nachricht zu senden.

#### Überprüfen Sie die Verbindung:
1.	Überprüfen Sie die Statusanzeige: In MQTT.fx gibt es normalerweise eine Statusanzeige, die den Verbindungsstatus anzeigt. Stellen Sie sicher, dass die Verbindung erfolgreich hergestellt wurde.
2.	Testen Sie mit einem MQTT-Client: Verwenden Sie einen anderen MQTT-Client, um sicherzustellen, dass Ihr Broker korrekt konfiguriert ist und Nachrichten empfangen kann.

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
