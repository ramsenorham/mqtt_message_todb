package mqtt_message;

import database.Util;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/*
 * INSERT INTO "public"."mqtt" ("id","zeitstempel","topic","message") VALUES (nextval('mqtt_id_seq'::regclass),'new Timestamp(System.currentTimeMillis())','test','test')
 */
public class Mqtt_message {
    
    private static Util util = new Util();
            
    private static Connection conn = null;
    
    public static void main(String[] args) {
        
        String broker = "tcp://192.168.178.44:1883";
        String clientId = "MQTT_FX_Client1";
        String topic = "sensor";
        int qos = 0;

        System.out.println("Start MQTT!");
        
        try {
            conn = util.getConnection("mqtt_db");
            util.databaseMetadata(conn);
            MqttClient mqttClient = new MqttClient(broker, clientId, new MemoryPersistence());
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            mqttClient.connect(connOpts);
            mqttClient.subscribe(topic, qos);
            mqttClient.setCallback(new MqttCallbackImpl());
        } catch (MqttException me) {
            System.out.println("reason " + me.getReasonCode());
            System.out.println("msg " + me.getMessage());
            System.out.println("loc " + me.getLocalizedMessage());
            System.out.println("cause " + me.getCause());
            System.out.println("excep " + me);
            me.printStackTrace();
        } catch (SQLException ex) {
            Logger.getLogger(Mqtt_message.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Mqtt_message.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Mqtt_message.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    static class MqttCallbackImpl implements MqttCallback {
        @Override
        public void connectionLost(Throwable cause) {
            System.out.println("Connection lost because: " + cause);
        }

        @Override
        public void messageArrived(String topic, MqttMessage message) throws Exception {
            String payload = new String(message.getPayload());
            System.out.println("Message arrived: " + payload + " from topic: " + topic);
            
            // Write the message to the database here.
            saveMessageToDatabase(topic, payload);
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken token) {
            System.out.println("Delivery complete.");
        }
        
        public void saveMessageToDatabase(String topic, String payload) {
            
            PreparedStatement statement = null;
            try {
            if (conn != null && !conn.isClosed()) {
                System.out.println("Connection Stabile.");
                String sql = "INSERT INTO mqtt (zeitstempel, topic, message) VALUES (?, ?, ?)";
        
                statement = conn.prepareStatement(sql);
                statement.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
                statement.setString(2, topic);
                statement.setString(3, payload);
                statement.executeUpdate();
            } else {
                conn.close();
                System.out.println("Connection wurde wieder geschlossen!");
            }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException se) {
                    se.printStackTrace();
                }
            }

        }
    }
}